# In dieser Datei befinden sich die Funktionen zum einheitlichen Aufrufen der Algorithmen und zum Untersuchen der Ergebnisse

import networkx as nx

# jedem der Knoten des Graphen wird eine Farbe zugeordnet, die der Liste entnommen wird. Ungefärbte Knoten erhalten die Farbe 0.
def initializeColorAttribute(G, Given):
    for i in range(G.number_of_nodes()):
        G.nodes[i]['color'] = Given[i]

# Überprüft ein Sudoku-Rätsel auf Verstöße gegen die Sudoku-Regeln. Hiermit können zum Beispiel Sudoku-Rätsel auf offensichtliche Unlösbarkeit überprüft werden und gelöste Rätsel auf Richtigkeit der Lösung 
# Wird ein Fehler gefunden, wird die Nummer des betreffenden Knoten ausgegeben. Je Durchlauf wird immer nur der erste Fehler angezeigt.
def SudokuChecker(SList, sizeN):
    S=nx.sudoku_graph(sizeN)
    initializeColorAttribute(S, SList)
    for n in S.nodes():
        currentColor = S.nodes()[n]['color']
        if (currentColor != 0) and (currentColor in list(S.nodes()[k]['color'] for k in S.neighbors(n))):
            print(n)
            return False
    return True

        
# Ein als Liste vorliegendes n-Sudoku-Rätsel mit dem gewünschten Algorithmus lösen
def solvePuzzleWith(Puzzle, puzzleSize, Algorithm):
    S=nx.sudoku_graph(puzzleSize)
    initializeColorAttribute(S, Puzzle)
    return Algorithm(S, puzzleSize)


# Die Elemente einer Liste von n-Sudoku-Rätseln der Reihe nach mit dem gleichen Algorithmus lösen lassen
def solvePuzzleSet (PuzzleSet, puzzleSize, Algorithm):
    results=[]
    for puzzle in PuzzleSet:
        results.append(solvePuzzleWith(puzzle, puzzleSize, Algorithm))
    return results


# Das ungelöste Sudoku-Rätsel im Vergleich zu dem Ergebnis des Algorithmus und der tatsächlichen Lösung des Rätsels
def analyseResult(Puzzle, Result, Solution):
    numberOfNodes= len(Solution)             # Anzahl der Zellen des Sudoku-Rätsels
    nodesToColor=Puzzle.count(0)             # Anzahl der Knoten, die der Algorithmus färben sollte
    nodesNotColored=Result.count(0)          # Anzahl der Knoten, die beim Ergebnis nicht gefärbt werden konnten
    nodesColoredCorrectly=-(numberOfNodes-nodesToColor)
    for i in range(len(Solution)):
        if Result[i] == Solution[i]:
            nodesColoredCorrectly+=1         # Anzahl der Knote, die richtig gefärbt wurden
    return [numberOfNodes, nodesToColor, nodesNotColored, nodesColoredCorrectly]


# Hiermit soll abgeschätzt werden, wie gut die gelieferten Ergebnisse für eine Liste von Rätseln sind
def getAverageSuccess(PuzzleSet, ResultSet, SolutionSet):
    numberOfTests= len(SolutionSet)        
    aNodesToColor=0                        
    aUncoloredNodes=0
    aNodesColoredcorrectly=0
    aProperColored=0
    for i in range(numberOfTests):
        success= analyseResult(PuzzleSet[i], ResultSet[i], SolutionSet[i])
        aNodesToColor+=success[1]
        aUncoloredNodes+=success[2]/success[1]
        aNodesColoredcorrectly+=success[3]/success[1]
        if ResultSet[i] == SolutionSet[i]:
            aProperColored += 1
 # [Anteil der zu Färbenden Knoten, Anteil der nicht färbbaren Knoten, Anteil der richtig gefärbten Knoten, Anteil richtig gelöster Rätsel]
    return [(aNodesToColor/numberOfTests)/len(SolutionSet[i]), aUncoloredNodes/numberOfTests, aNodesColoredcorrectly/numberOfTests, aProperColored/numberOfTests]