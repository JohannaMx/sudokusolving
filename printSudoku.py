# In diesem Dokument befinden sich die Funktionen zum übersichtlicheren Ausgeben der in Listenform vorliegenden Sudoku-Rätsel.

# Printet ein 2-Sudoku-Rätsel
def printSudoku4x4fromList(ValueList):
    # ersetzt für die bessere Lesbarkeit alle Felder mit 0 durch '_'
    ValueList=[v if v != 0 else '_' for v in ValueList]
    print(ValueList[0], ValueList[1],'|', ValueList[2],ValueList[3])
    print(ValueList[4], ValueList[5],'|', ValueList[6],ValueList[7])
    print('---------')
    print(ValueList[8], ValueList[9],'|', ValueList[10],ValueList[11])
    print(ValueList[12], ValueList[13],'|', ValueList[14],ValueList[15])

# Printet ein 3-Sudoku-Rätsel
def printSudoku9x9fromList(ValueList): 
    ValueList=[v if v != 0 else '_' for v in ValueList]
    print(ValueList[0], ValueList[1], ValueList[2],'|',ValueList[3], ValueList[4], ValueList[5], '|', ValueList[6], ValueList[7], ValueList[8])
    print(ValueList[9], ValueList[10], ValueList[11], '|',ValueList[12], ValueList[13], ValueList[14],'|',ValueList[15], ValueList[16], ValueList[17])
    print(ValueList[18], ValueList[19], ValueList[20],'|',ValueList[21], ValueList[22], ValueList[23], '|',ValueList[24], ValueList[25], ValueList[26])
    print('---------------------')
    print(ValueList[27], ValueList[28], ValueList[29], '|', ValueList[30], ValueList[31], ValueList[32],'|',ValueList[33], ValueList[34], ValueList[35])
    print(ValueList[36], ValueList[37], ValueList[38], '|', ValueList[39], ValueList[40], ValueList[41],'|',ValueList[42], ValueList[43], ValueList[44])
    print(ValueList[45], ValueList[46], ValueList[47], '|', ValueList[48], ValueList[49], ValueList[50],'|',ValueList[51], ValueList[52], ValueList[53])
    print('---------------------')
    print(ValueList[54], ValueList[55], ValueList[56], '|', ValueList[57], ValueList[58], ValueList[59],'|',ValueList[60], ValueList[61], ValueList[62])
    print(ValueList[63], ValueList[64], ValueList[65], '|', ValueList[66], ValueList[67], ValueList[68],'|',ValueList[69], ValueList[70], ValueList[71])
    print(ValueList[72], ValueList[73], ValueList[74], '|', ValueList[75], ValueList[76], ValueList[77],'|',ValueList[78], ValueList[79], ValueList[80])

# Printet ein 4-Sudoku-Rätsel
def printSudoku16x16fromList(ValueList):
    ValueList=[v if v != 0 else '__' for v in ValueList]
    ValueList=[v if v in [10,11,12,13,14,15,16, '__'] else ' '+str(v) for v in ValueList]
    print(ValueList[0],   ValueList[1], ValueList[2],   ValueList[3],'|', ValueList[4], ValueList[5],  ValueList[6],   ValueList[7], '|',ValueList[8],  ValueList[9], ValueList[10], ValueList[11], '|',ValueList[12], ValueList[13], ValueList[14], ValueList[15])
    print(ValueList[16], ValueList[17], ValueList[18], ValueList[19],'|', ValueList[20],ValueList[21], ValueList[22], ValueList[23], '|',ValueList[24], ValueList[25], ValueList[26],ValueList[27], '|', ValueList[28], ValueList[29], ValueList[30], ValueList[31])
    print(ValueList[32], ValueList[33], ValueList[34], ValueList[35],'|',ValueList[36], ValueList[37], ValueList[38], ValueList[39], '|', ValueList[40], ValueList[41],ValueList[42], ValueList[43],'|', ValueList[44], ValueList[45], ValueList[46], ValueList[47])
    print( ValueList[48], ValueList[49], ValueList[50],ValueList[51],'|',ValueList[52], ValueList[53], ValueList[54], ValueList[55], '|', ValueList[56], ValueList[57], ValueList[58], ValueList[59],'|',ValueList[60], ValueList[61], ValueList[62], ValueList[63])
    print('-----------------------------------------------------')
    print(ValueList[64], ValueList[65], ValueList[66], ValueList[67],'|', ValueList[68], ValueList[69],ValueList[70], ValueList[71], '|',ValueList[72], ValueList[73], ValueList[74], ValueList[75], '|',ValueList[76], ValueList[77], ValueList[78], ValueList[79])
    print(ValueList[80], ValueList[81], ValueList[82], ValueList[83],'|', ValueList[84], ValueList[85], ValueList[86], ValueList[87], '|',ValueList[88], ValueList[89], ValueList[90],ValueList[91], '|', ValueList[92], ValueList[93], ValueList[94], ValueList[95])
    print(ValueList[96], ValueList[97], ValueList[98], ValueList[99],'|',ValueList[100], ValueList[101], ValueList[102], ValueList[103], '|', ValueList[104], ValueList[105],ValueList[106], ValueList[107],'|', ValueList[108], ValueList[109], ValueList[110], ValueList[111])
    print(ValueList[112], ValueList[113], ValueList[114],ValueList[115],'|',ValueList[116], ValueList[117], ValueList[118], ValueList[119], '|', ValueList[120], ValueList[121], ValueList[122], ValueList[123],'|',ValueList[124], ValueList[125], ValueList[126], ValueList[127])
    print('-----------------------------------------------------')
    print(ValueList[128], ValueList[129], ValueList[130], ValueList[131],'|', ValueList[132], ValueList[133],ValueList[134], ValueList[135], '|',ValueList[136], ValueList[137], ValueList[138], ValueList[139], '|',ValueList[140], ValueList[141], ValueList[142], ValueList[143])
    print(ValueList[144], ValueList[145], ValueList[146], ValueList[147],'|', ValueList[148], ValueList[149], ValueList[150], ValueList[151], '|',ValueList[152], ValueList[153], ValueList[154],ValueList[155], '|', ValueList[156], ValueList[157], ValueList[158], ValueList[159])
    print(ValueList[160], ValueList[161], ValueList[162], ValueList[163],'|',ValueList[164], ValueList[165], ValueList[166], ValueList[167], '|', ValueList[168], ValueList[169],ValueList[170], ValueList[171],'|', ValueList[172], ValueList[173], ValueList[174], ValueList[175])
    print(ValueList[176], ValueList[177], ValueList[178], ValueList[179],'|',ValueList[180], ValueList[181], ValueList[182], ValueList[183], '|', ValueList[184], ValueList[185], ValueList[186], ValueList[187],'|',ValueList[188], ValueList[189], ValueList[190], ValueList[191])
    print('-----------------------------------------------------')
    print(ValueList[192], ValueList[193], ValueList[194], ValueList[195],'|', ValueList[196], ValueList[197],ValueList[198], ValueList[199], '|',ValueList[200], ValueList[201], ValueList[202], ValueList[203], '|',ValueList[204], ValueList[205], ValueList[206], ValueList[207])
    print(ValueList[208], ValueList[209], ValueList[210], ValueList[211],'|', ValueList[212], ValueList[213], ValueList[214], ValueList[215], '|',ValueList[216], ValueList[217], ValueList[218],ValueList[219], '|', ValueList[220], ValueList[221], ValueList[222], ValueList[223])
    print(ValueList[224], ValueList[225], ValueList[226], ValueList[227],'|',ValueList[228], ValueList[229], ValueList[230], ValueList[231], '|', ValueList[232], ValueList[233],ValueList[234], ValueList[235],'|', ValueList[236], ValueList[237], ValueList[238], ValueList[239])
    print(ValueList[240], ValueList[241], ValueList[242], ValueList[243],'|',ValueList[244], ValueList[245], ValueList[246], ValueList[247], '|', ValueList[248], ValueList[249], ValueList[250], ValueList[251],'|',ValueList[252], ValueList[253], ValueList[254], ValueList[255])

# Diese Funktion ruft je nach übergebenem n die entsprechende Funktion zum printen auf.
def printSudokuFromList(ValueList, sizeN):
    if sizeN==2:
        printSudoku4x4fromList(ValueList)
    elif sizeN==3:
        printSudoku9x9fromList(ValueList)
    elif sizeN==4:
        printSudoku16x16fromList(ValueList)
    