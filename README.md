## SudokuSolving
Programm, bei dem das Lösen von Sudoku-Rätseln mithilfe von Graphenfärbung in Python realisiert wurde. Zum Finden der Graphenfärbungen stehen der Backtracking- und der Greedy-Algorithmus zur Verfügung. Die ermittelten Lösungen der Sudoku-Rätsel können auf Korrektheit, Vollständigkeit und Schnelligkeit untersucht werden.

Entstanden ist dieses Projekt im Rahmen meiner Bachelorabeit "Vergleichende Implementierung von Färbealgorithmen zum Lösen von Sudokus" an der HAW Hamburg.

## Ablaufdiagramm Greedy-Algorithmus

![Greedy Ablaufdiagramm](images/PAP_Greedy.png)

## Ablaufdiagramm Backtracking-Algorithmus

![Backtracking Ablaufdiagramm](images/PAP_Backtracking.png)
